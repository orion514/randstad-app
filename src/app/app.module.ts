//  Angular 
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Router, Routes } from '@angular/router';
import {LocationStrategy} from '@angular/common';

// bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PopoverBasicComponent } from './components/bootstrap/popover-basic/popover-basic.component';

// Main component
import { AppComponent } from './app.component';

// services
import {AUTH_PROVIDERS} from './services/AuthService';
import {LoggedInGuard} from './guards/loggedIn.guard';

// Nav
import {LoginComponent} from './components/LoginComponent';
import {HomeComponent} from './components/HomeComponent';
import {AboutComponent} from './components/AboutComponent';
import {ContactComponent} from './components/ContactComponent';
import {ProtectedComponent} from './components/ProtectedComponent';

const routes: Routes = [
  { path: '',          redirectTo: 'home', pathMatch: 'full' },
  { path: 'home',      component: HomeComponent },
  { path: 'about',     component: AboutComponent },
  { path: 'contact',   component: ContactComponent },
  { path: 'protected', component: ProtectedComponent,
    canActivate: [LoggedInGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    PopoverBasicComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    ProtectedComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(routes),
    HttpModule
  ],
  providers: [
    AUTH_PROVIDERS,
    LoggedInGuard,],
  bootstrap: [AppComponent]
})
export class AppModule { }